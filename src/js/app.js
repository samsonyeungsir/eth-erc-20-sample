



App = {
    contracts: {},
    account: '0x0',
    web3Provider: null,
    loading: false,
    tokenPrice: 11113456755566,
balance:0,
tokenSold:0,
tokensAvailable:7500,
    init: function () {
        console.log("App initialized")
        return App.initWeb3();

    },
    initWeb3: function () {

        const Web3 = require('web3')
        if (typeof web3 !== 'undefined') {

            ethereum.enable().then(() => {
                web3 = new Web3(web3.currentProvider);

            });

        } else {

            // If no injected web3 instance is detected, fallback to the TestRPC

            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
        }

        App.web3Provider = web3.currentProvider;



        return App.initContracts();
    },
    initContracts: function () {
        $.getJSON("samsoncointokensale.json", function (samsoncointokensale) {
            App.contracts.samsoncointokensale = TruffleContract(samsoncointokensale)
            App.contracts.samsoncointokensale.setProvider(App.web3Provider)
            App.contracts.samsoncointokensale.deployed().then(function (samsoncointokensale) {
                console.log('token sale address', samsoncointokensale.address);
            });
        }).done(function () {
            console.log("done function")
            $.getJSON("samsoncoin.json", function (samsoncoin) {

                App.contracts.samsoncoin = TruffleContract(samsoncoin)
                App.contracts.samsoncoin.setProvider(App.web3Provider)
                App.contracts.samsoncoin.deployed().then(function (samsoncoin) {
                    console.log('token sale address', samsoncoin.address);
                });
                App.listenFOrEvents();
                return App.render();
            })

        });

    },

listenFOrEvents:function(){
App.contracts.samsoncointokensale.deployed().then(function(instance){
instance.Sell({},{
fromBlock:0,
toBlock:'lastest',
}).watch(function(error,event){
console.log("event triggered",event);
App.render();
})
})
},

    render: function () {
        if (App.loading) {
            return;
        }
        App.loading = true;

        var loader = $('#loader');
        var content = $('#content');

        loader.show();
        content.hide();

        //load data
        web3.eth.getCoinbase(function (err, account) {
            if (err === null) {
                App.account = account;
                $('#accountAddress').html("your address:  " + account);

            }
        })

        App.contracts.samsoncointokensale.deployed().then(function (instance) {
            samsoncointokensaleInstance = instance;
            return samsoncointokensaleInstance.tokenPrice();
        }).then(function (tokenPrice) {
            console.log("tokenprice", tokenPrice)
            App.tokenPrice = tokenPrice;
            $('.token-price').html(web3.fromWei(App.tokenPrice,"ether").toNumber());
            return samsoncointokensaleInstance.tokensSold();
        }).then(function(tokensSold){
            App.tokenSold = tokensSold.toNumber();
$('.tokens-sold').html(App.tokenSold);
            $('.tokens-available').html(App.tokensAvailable);

var progressPercent = (App.tokenSold/App.tokensAvailable)*100
$('#progress').css('width',progressPercent +'%');

App.contracts.samsoncoin.deployed().then(function(instance){
samsoncoinInstance = instance;

    return samsoncoinInstance.balanceOf(App.account);
}).then(function(balance){
App.balance= balance
    console.log("dapp-balance", App.balance)
balance
    $('.dapp-balance').html(balance.toNumber());
})

});

        App.loading = false;
        loader.hide();
        content.show();

    },

 buyTokens: function() {
$('#content').hide();
    $('#loader').show();
var numberOfTokens =$('#numberOfTokens').val();
App.contracts.samsoncointokensale.deployed().then(function(instance){
    console.log("tokenPrice", App.tokenPrice.toNumber(),)
    console.log("No.of Token", numberOfTokens,)
    console.log("value", String(numberOfTokens * App.tokenPrice.toNumber()))
    console.log("Account", App.account,)
    return instance.buyTokens(numberOfTokens, {
        from: App.account, value: numberOfTokens * App.tokenPrice,
//gas limied
gas:5000000
})
}).then(function(result){
console.log("token brought...")
    $('form').trigger('reset')
 //Wait for sell evernt

})
}

}


$(function () {
    $(window).load(function () {
        App.init();

    })

});


//const { default: Web3 } = require("web3");

// var BigNumber = require('bignumber.js');
// App = {

//     web3Provider: null,
// account:"0x0",
//     contracts: {},
// loading:false,
//     tokenPrice: 11113456755566,
// tokensSold:0,
// tokensAvailable:750000,

//     init: function () {
//         console.log("App initialized...")
//         return App.initWeb3();
//     },


//     initWeb3: function () {
//         const Web3 = require('web3')
//         if (typeof web3 !== 'undefined') {

//             ethereum.enable().then(() => {
//                 web3 = new Web3(web3.currentProvider);

//             });

//         } else {

//             // If no injected web3 instance is detected, fallback to the TestRPC

//             web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
//         }

//         App.web3Provider = web3.currentProvider;



//         return App.initContracts();
//     },



// App.listenForEvents();
// return App.render();
//             })
//         })

//     },

// //Listen for events emitted from the contract
// listenForEvents:function(){
// App.contracts.samsoncointokensale.deployed().then(function(instance){
// instance.Sell({},{fromBlock:0,toBlock:'latest',}).watch(function(error,event){
// console.log("event triggered",event);
// App.render();
// })

// })

// },



// render: function(){
// if (App.loading){
// return;
// }
// App.loading=true;
// var loader =$('#loader')
// var content = $('#content')

// loader.show();
// content.hide();

// //Load account data
// web3.eth.getCoinbase(function(err,account){
// if(err===null){
// App.account=account;
// $('#accountAddress').html("Your Account "+account);

// }
// })
// //Load token sale contra
// App.contracts.samsoncointokensale.deployed().then(function(instance){
// samsoncointokensaleInstance= instance;
// return samsoncointokensaleInstance.tokenPrice();

// }).then(function(tokenPrice){
// console.log("token price",tokenPrice);
//     App.tokenPrice = tokenPrice;
//     $('.token-price').html(web3.fromWei(App.tokenPrice,"ether").toNumber());
// return samsoncointokensaleInstance.tokensSold();


// }).then(function(tokensSold){
//     App.tokensSold = tokensSold.toNumber();
//     console.log('tokenSold', App.tokensSold)
//     $('.tokens-sold').html(App.tokensSold);
// $('.tokens-available').html(App.tokensAvailable)

// var progressPercent = (App.tokensSold/App.tokensAvailable)*100;
// $('#progress').css('width',progressPercent+'%');

// //Load token contract
// App.contracts.samsoncoin.deployed().then(function(instance){
// samsoncoinInstance=instance;
// return samsoncoinInstance.balanceOf(App.account);
// }).then(function(balance){
// $('.dapp-balance').html(balance.toNumber())

//     App.loading = false;
//     loader.hide();
//     content.show();
// } )
// })



// },
// buyTokens: function(){
// $('#content').hide();
//     $('#loader').show();
// var numberOfTokens =$('#numberOfTokens').val();
// App.contracts.samsoncointokensale.deployed().then(function(instance){
//     console.log("tokenPrice", App.tokenPrice.toNumber(),)
//     console.log("No.of Token", numberOfTokens,)
//     console.log("value", String(numberOfTokens * App.tokenPrice.toNumber()))
//     console.log("Account", App.account,)
//     return instance.buyTokens(numberOfTokens, {
//         from: App.account, value: numberOfTokens * App.tokenPrice,
// //gas limied
// gas:5000000
// })
// }).then(function(result){
// console.log("token brought...")
// $('form').trigger('reset')
//  //Wait for sell evernt

// })
// }

// }






// $(function () {
//     $(window).load(function () {
//         App.init();

//     })

// });