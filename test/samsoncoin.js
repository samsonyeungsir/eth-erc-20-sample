var samsoncoin = artifacts.require("./samsoncoin.sol");

contract('samsoncoin', function (accounts) {
    var tokenInstance

    it('initializes the contract with correct value', function () {
        return samsoncoin.deployed().then(function (instance) {
            tokenInstance = instance;
            return tokenInstance.name();
        }).then(function (name) {
            assert.equal(name, 'samsoncoin', 'has a correct name');
            return tokenInstance.symbol();

        }).then(function (symbol) {
            assert.equal(symbol, 'SAM', 'has a correct symbol')
            return tokenInstance.standard();
        }).then(function (standard) {
            assert.equal(standard, 'SAM v1.0', 'has the correct standard');
        })
    })
    it('allocates the initial supply upon deployment', function () {
        return samsoncoin.deployed().then(function (instance) {
            tokenInstance = instance;
            return tokenInstance.totalSupply();
        }).then(function (totalSupply) {
            assert.equal(totalSupply.toNumber(), 7000000, 'sets the total supply to 7M');
            return tokenInstance.balanceOf(accounts[0]);
        }).then(function (adminBalance) {
            assert.equal(adminBalance.toNumber(), 7000000, 'it allocates to admin')
        });
    });


    it('transfers token ownership', function () {
        return samsoncoin.deployed().then(function (instance) {
            tokenInstance = instance;
            // Test `require` statement first by transferring something larger than the sender's balance
            return tokenInstance.transfer.call(accounts[1], 99999999999999999999999);
        }).then(assert.fail).catch(function (error) {
            
            return tokenInstance.transfer.call(accounts[1], 250000, { from: accounts[0] });
        }).then(function (success) {
            assert.equal(success, true, 'it returns true');
            return tokenInstance.transfer(accounts[1], 250000, { from: accounts[0] });
        }).then(function (receipt) {
            assert.equal(receipt.logs.length, 1, 'triggers one event');
            assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
            assert.equal(receipt.logs[0].args._from, accounts[0], 'logs the account the tokens are transferred from');
            assert.equal(receipt.logs[0].args._to, accounts[1], 'logs the account the tokens are transferred to');
            assert.equal(receipt.logs[0].args._value, 250000, 'logs the transfer amount');
            return tokenInstance.balanceOf(accounts[1]);
        }).then(function (balance) {
            assert.equal(balance.toNumber(), 250000, 'adds the amount to the receiving account');
            return tokenInstance.balanceOf(accounts[0]);
        }).then(function (balance) {
            assert.equal(balance.toNumber(), 6750000, 'deducts the amount from the sending account');
        });
    });

    it('approve token', function () {
        return samsoncoin.deployed().then(function (instance) {
            tokenInstance = instance;
            return tokenInstance.approve.call(accounts[1], 100);
        }).then(function (success) {
            assert.equal(success, true, 'it returns true');
            return tokenInstance.approve(accounts[1], 100, { from: accounts[0] });

        }).then(function (receipt) {
            assert.equal(receipt.logs.length, 1, 'triggers one event');
            assert.equal(receipt.logs[0].event, 'Approval', 'should be the "Transfer" event');
            assert.equal(receipt.logs[0].args._owner, accounts[0], 'logs the account the tokens are transferred from');
            assert.equal(receipt.logs[0].args._spender, accounts[1], 'logs the account the tokens are transferred to');
            assert.equal(receipt.logs[0].args._value, 100, 'logs the transfer amount');
            return tokenInstance.allowance(accounts[0], accounts[1]);
        }).then(function (allowance) {
            assert.equal(allowance.toNumber(), 100, 'stores the allowance for delegrated ');
        })
    })
    it('transfer from', function () {
        return samsoncoin.deployed().then(function (instance) {
            tokenInstance = instance;
            fromAccount = accounts[2];
            toAccount = accounts[3];
            spendingAccount = accounts[4];
            //Transfer some tokens to fromAccount
            return tokenInstance.transfer(fromAccount, 100, { from: accounts[0] });

        }).then(function (receipt) {
            //Approve spendingAccount to spend10 tokens from fromAccount
            return tokenInstance.approve(spendingAccount, 10, { from: fromAccount });
        }).then(function (receipt) {
            return tokenInstance.transferFrom(fromAccount, toAccount, 999, { from: spendingAccount })
        }).then(assert.fail).catch(function (error) {
            assert(error.message.indexOf('revert') >= 0, 'cannot larger than amount');
            //transfer larger than the amount
           
            return tokenInstance.transferFrom(fromAccount, toAccount, 20, { from: spendingAccount });
        }).then(assert.fail).catch(function (error) {
            assert(error.message.indexOf('revert') >= 0, 'larger than approve amounts');
            return tokenInstance.transferFrom.call(fromAccount, toAccount, 10, { from: spendingAccount })
        
        }).then(function(success){
            assert.equal(success,true);
            return tokenInstance.transferFrom(fromAccount, toAccount, 10, { from: spendingAccount });
}).then(function(receipt){
    assert.equal(receipt.logs.length, 1, 'triggers one event');
    assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
    assert.equal(receipt.logs[0].args._from, fromAccount, 'logs the account the tokens are transferred from');
    assert.equal(receipt.logs[0].args._to, toAccount, 'logs the account the tokens are transferred to');
    assert.equal(receipt.logs[0].args._value, 10, 'logs the transfer amount');
return tokenInstance.balanceOf(fromAccount);
}).then(function(balance){
assert.equal(balance.toNumber(),90,'deduct from balance account')
return tokenInstance.balanceOf(toAccount);
}).then(function(balance){
    assert.equal(balance.toNumber(), 10, 'add amount from receive amount');
return tokenInstance.allowance(fromAccount,spendingAccount);

}).then(function(allowance){
assert.equal(allowance,0,'deducts the amount from allowance')
})

    })

   


})


