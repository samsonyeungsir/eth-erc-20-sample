// SPDX-License-Identifier: MIT

pragma solidity >=0.4.22 <0.9.0;

import "./samsoncoin.sol";

contract samsoncointokensale {
address admin;
samsoncoin public tokenContract;
uint256 public tokenPrice;
uint256 public tokensSold;

event Sell(address _buyer,uint256 _amount);


constructor(samsoncoin _tokenContract, uint256 _tokenPrice ) public {
     //Assign an admin
admin= msg.sender;
tokenContract = _tokenContract;
tokenPrice= _tokenPrice;
//Token contract

//tokenprice   
    }
//buy Tokens
//mulitply
function multiply(uint x, uint y)internal pure returns(uint z){
require(y== 0 || (z=x*y)/y == x);
}

function buyTokens(uint256 _numberOfTokens) public payable{

//Require that value is equal
require(msg.value ==  multiply(_numberOfTokens, tokenPrice));
//Require that the contract has enought tokens
require(tokenContract.balanceOf(address(this)) >= _numberOfTokens);
require(tokenContract.transfer(msg.sender,_numberOfTokens));
//keep track of tokensSold 
tokensSold += _numberOfTokens;
//trigger sell Event

emit Sell(msg.sender, _numberOfTokens);

}
//ending token sell

function endSale() public payable{
//require admin
require(msg.sender == admin);
require(tokenContract.transfer(admin,tokenContract.balanceOf(address(this))));
//transfer remaining token to admin
//destroy contract
//selfdestruct(msg.sender);
//msg.sender.transfer(address(this).balance);

}


}